# Based on https://github.com/renovatebot/docker-renovate/blob/e43e650266bad3a5351e721d57c567f06c11f980/Dockerfile
FROM renovate/buildpack:6@sha256:009162804484f5fbc07e08de0dc4b00933ebb8db789753523d82c8e88ccaa6cf AS base

LABEL name="renovate"
LABEL org.opencontainers.image.source="https://github.com/renovatebot/renovate" \
  org.opencontainers.image.url="https://renovatebot.com" \
  org.opencontainers.image.licenses="AGPL-3.0-only"

# These versions are not updated by renovate. We should keep up with them alongside upstream renovate
RUN install-tool node v16.17.0
RUN install-tool yarn 1.22.19

WORKDIR /usr/src/app

COPY . .

RUN set -ex; \
  yarn install; \
  yarn build; \
  chmod +x dist/*.js;

# hardcode node version to renovate
RUN set -ex; \
  NODE_VERSION=$(node -v | cut -c2-); \
  sed -i "1 s:.*:#\!\/opt\/buildpack\/tools\/node\/${NODE_VERSION}\/bin\/node:" "dist/renovate.js"; \
  sed -i "1 s:.*:#\!\/opt\/buildpack\/tools\/node\/${NODE_VERSION}\/bin\/node:" "dist/config-validator.js";

# For renovate to be able to work with docker images we need docker installed in the image
RUN install-tool docker 20.10.9

RUN chmod +x bin/* && \
  cp bin/* /usr/local/bin/

# Validate installation
RUN set -ex; \
  renovate --version; \
  renovate-config-validator

CMD ["renovate"]

# Numeric user ID for the ubuntu user. Used to indicate a non-root user to OpenShift
USER 1000
